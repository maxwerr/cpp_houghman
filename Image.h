/*
  Image.h

  This is a modified version of Gady Agam's code from his
  "Introduction to programming with OpenCV" which was found at:
  http://www.cs.iit.edu/%7Eagam/cs512/lect-notes/opencv-intro/index.html

  Modified by Michael Eckmann 20080219
    - added code to release the IplImage
    - changed b, g and r to blue, green and red
    - changed Rgb to RGB
    - changed Bw to Grey
    - added function to return the imgp getImgPtr()
*/
#ifndef IMAGE2_H_
#define IMAGE2_H_

#include "cv.h"
#include "highgui.h"
#include <iostream>

using namespace std;

template<class T> class Image
{
	private:
		IplImage *imgp;
		CvSize cvSize;
	public:
		void setSizes(IplImage* image){
			CvSize imageSize;
			imageSize.height = image->height;
			imageSize.width = image->width;
			cvSize = imageSize;
		}
		
		Image(IplImage *img=0)
		{	
			imgp = img;
			setSizes(img);
		}
		Image(CvSize mySize, int type, int channel)
		{	
			imgp = cvCreateImage(mySize, type, channel);
			cvSize = mySize;
		}
		Image(int row, int col, int type, int channel)
		{	
			CvSize mySize = cvSize(row, col);
			imgp = cvCreateImage(mySize, type, channel);
			cvSize = mySize;
		}
		Image(string s, int channel)
		{	
			imgp = cvLoadImage(s.c_str(), channel);
			setSizes(imgp);
		}
		
		~Image()
		{
			cvReleaseImage(&imgp); // added this to release the image
			imgp = 0;
		}
		
		void operator = (IplImage *img)
		{
			imgp = img;
			setSizes(img);
		}
		
		inline T* operator [] (const int rowIndx) const
		{
			return ((T*) (imgp->imageData + rowIndx * imgp->widthStep));
		}

		IplImage* getImgPtr() const
		{
			return imgp;
		}
		
		CvSize getSize() const
		{
			return cvSize;
		}
};

typedef struct
{
	unsigned char blue, green, red;
} RGBPixel;

typedef struct
{
	float blue, green, red;
} RGBPixelFloat;

typedef Image<RGBPixel>       RGBImage;
typedef Image<RGBPixelFloat>  RGBImageFloat;
typedef Image<unsigned char>  GreyImage;
typedef Image<float>          GreyImageFloat;

#endif /*IMAGE2_H_*/
