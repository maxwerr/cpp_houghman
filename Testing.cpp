#include "Image.h"
#include "cell.h"
#include "PTList.h"
#include "Hough.h"


using namespace std;

int main(){
	IplImage* myImg = cvLoadImage("box.jpg", 1);
	GreyImage* myGrey = new GreyImage(myImg);
	Hough* myHough = new Hough(myGrey, 400, true, 10, 3);
	
	myHough->accumulate();
	
	PTList* myPT = myHough->getPT();
	cell* myTop = myPT->getTop();
	
	for(int i = 0; i < 9; i++){
		myTop[i].print();
	}
	
	return 0;
}
