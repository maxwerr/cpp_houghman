#include "Image.h"
#include <iostream>
#include <math.h>
#include "Hough.h"

const double PI = 3.14159262535;

void Hough::accumulate(){
	
	int originRow = 1;
	int originCol = 1;
	
	CvPoint here = cvPoint(0,0);
	
	for(int r = rowStart; r < rowEnd; r++){
		for(int c = colStart; c < colEnd; c++){
			
			//cout << r << " is r and " << c << " is c" << endl;
			
			float mag = 0;
			float rgrad = 0;
			float cgrad = 0;
			
			for(int maskR = 0; maskR < 3; maskR++){
				for(int maskC = 0; maskC < 3; maskC++){
					
					int subR = r - originRow + maskR;
					int subC = c - originCol + maskC;
					
					if(subR < 0)							
						subR = -subR;
					if(subC < 0)
						subC = -subC;
					
					cgrad += ((*img)[subR][subC])*((x_mask)[maskR][maskC]);
				}
			}
			
			for(int maskR = 0; maskR < 3; maskR++){
				for(int maskC = 0; maskC < 3; maskC++){
					
					int subR = r - originRow + maskR;
					int subC = c - originCol + maskC;
					
					if(subR < 0)							
						subR = -subR;
					if(subC < 0)
						subC = -subC;
					
					rgrad += ((*img)[subR][subC])*((y_mask)[maskR][maskC]);
				}
			}
			
			mag = sqrt((cgrad*cgrad) + (rgrad*rgrad));
			
			if(mag > gradThresh){
				
				double theta = atan2(rgrad,cgrad);
				
					//degress from radians
				int thetaq = (int)(theta * 57.29578)+(180);
				int distq = (int)(c*cos(thetaq) - r*(sin(thetaq)));
				
					//quantize to nearest angle
				thetaq /= angleInc;
				thetaq *= angleInc;
				
				if(distq < 0){
					distq = -distq;
				}
				
					//quantize
				distq /= distInc;
				distq *= distInc;
				
				//cout << "about to HERE" << endl;
				
				here.x = c;
				here.y = r;
				
				
				//cout << "distq = " << distq << "\tthetaq = " << thetaq << "\tmag = " << mag << "\there.x = " << here.x << "\there.y = " << here.y << endl;
				
				myPTList->addToArray(distq, thetaq, mag, here);
			}
		}
	}
	cout << "Accumulated" << endl;
}

Hough::Hough(GreyImage* myGrey, int thresh, bool sobel, int angleIn, int distIn){
	angleInc = angleIn;
	distInc = distIn;
	gradThresh = thresh;
	img = myGrey;
	
	inputSize = img->getSize();
	
	rowStart = 0;
	rowEnd = inputSize.height;
	colStart = 0;
	colEnd = inputSize.width;
	
	if(sobel){
		x_mask = getSobel(true);
		y_mask = getSobel(false);
	}
	else{
		x_mask = getPrewitt(true);
		y_mask = getPrewitt(false);
	}

	buildPT();
}

float** Hough::getPrewitt(bool x){
	float **mask = new float*[3];
	for(int i = 0; i < 3; i++)
		mask[i] = new float[3];
	if (x) {
		mask[0][0] = -1;
		mask[0][1] = -1;
		mask[0][2] = -1;

		mask[1][0] = 0;
		mask[1][1] = 0;
		mask[1][2] = 0;

		mask[2][0] = 1;
		mask[2][1] = 1;
		mask[2][2] = 1;
	}
	else
	{
		mask[0][0] = -1;
		mask[1][0] = -1;
		mask[2][0] = -1;

		mask[0][1] = 0;
		mask[1][1] = 0;
		mask[2][1] = 0;

		mask[0][2] = 1;
		mask[1][2] = 1;
		mask[2][2] = 1;
	}
	
	return mask;
}

float** Hough::getSobel(bool x){
	float **mask = new float*[3];
	for(int i = 0; i < 3; i++)
		mask[i] = new float[3];
	if (x) {
		mask[0][0] = -1;
		mask[0][1] = -2;
		mask[0][2] = -1;

		mask[1][0] = 0;
		mask[1][1] = 0;
		mask[1][2] = 0;

		mask[2][0] = 1;
		mask[2][1] = 2;
		mask[2][2] = 1;
	}
	else
	{
		mask[0][0] = -1;
		mask[1][0] = -2;
		mask[2][0] = -1;

		mask[0][1] = 0;
		mask[1][1] = 0;
		mask[2][1] = 0;

		mask[0][2] = 1;
		mask[1][2] = 2;
		mask[2][2] = 1;
	}
	
	return mask;
}
