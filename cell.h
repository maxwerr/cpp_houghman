#ifndef CELL_H_
#define CELL_H_

#include <vector>
#include "Image.h"
#include <math.h>

class cell{
private:
	vector<CvPoint> points;
	float mag;
public:
	cell(){
		CvPoint location = cvPoint(0,0);
		points.push_back(location);		
		mag = 0;
	}
	void add(float m, CvPoint here){
		mag += m;
		points.push_back(here);
	}
	vector<CvPoint> getPoint(){
		return points;
	}
	float getMag(){
		return mag;
	}
	void setMag(float magi){
		mag = magi;
	}
	void print(){
		cout << "In this cell:" << endl;
		for(int i = 0; i < points.size(); i++){
			cout << "Point #" << i << ": " << points[i].x << ", " << points[i].y << endl;;
		}
		cout << "MAG: " << mag << "\n" << endl;
	}
};

#endif /*CELL_H_*/
