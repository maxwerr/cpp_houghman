#ifndef PTLIST_H_
#define PTLIST_H_
#include <vector>
#include "cell.h"

using namespace std;

class PTList{
private:
	cell** array;
	int dq, thetaq;
	int dInc, aInc;
	cell top9[9];
public:
	PTList(int d, int th, int distInc, int angleInc){
		dq = d;
		thetaq = th;
		dInc = distInc;
		aInc = angleInc;
		array = new cell*[dq];
		for(int i = 0; i < dq; i++){
			array[i] = new cell[thetaq];
		}
		cout << "this is in PTList" << endl;
		for(int i = 0; i < 9; i++){
			top9[i].setMag(0);
		}
	}
	void updateTop9(cell myCell){
		float updateMag = myCell.getMag();
		
		for(int i = 0; i < 9; i++){
			if(updateMag > top9[i].getMag()){
				top9[i] = myCell;
				break;
			}
		}
	}
	void addToArray(int distq, int thetaq, float mag, CvPoint here){
		distq /= dInc;
		thetaq /= aInc;
		array[distq][thetaq].add(mag, here);
		array[distq][thetaq].print();
	}
	cell* getTop(){
		return top9;
	}
	
};

#endif /*PTLIST_H_*/
