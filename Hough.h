#ifndef HOUGH_H_
#define HOUGH_H_

#include "PTList.h"
#include "Image.h"

using namespace std;

class Hough{
	
private:
	GreyImage* img;
	float** x_mask;
	float** y_mask;
	int rowStart, rowEnd, colStart, colEnd;
	CvSize inputSize;
	int dSize, thSize; 
	int gradThresh; 
	int angleInc, distInc;
	PTList* myPTList;
public:
	Hough(GreyImage* myGrey, int gradThresh, bool sobel, int angleIn, int distIn);
	
	void buildPT(){
		
		int Asqr = (int)pow(inputSize.width, 2);
		int Bsqr = (int)pow(inputSize.height, 2);
		int C = (int)sqrt(Asqr + Bsqr);				//maximum distance
		
		int remainder = C % distInc;			//how much left over
		
		C = C - remainder + distInc;			//rounds to next dist Increment
		
		dSize = C / distInc;					//dimensions needed
		thSize = 360 / angleInc;
		
		myPTList = new PTList(dSize, thSize, distInc, angleInc);
	}
	float** getPrewitt(bool x);
	float** getSobel(bool x);
	void accumulate();
	PTList* getPT(){
		return myPTList;
	}
};

#endif /*HOUGH_H_*/
