#include <iostream>
#include "highgui.h"
#include <vector>
#include "math.h"

//and other things

class line{
	private:
		vector<CvPoint> ptlist;
		cvScalar color;
		IplImage img;
	public:
		line(vector<CvPoint> ptlist, cvScalar color){
			this.ptlist = ptlist;
			this.color = color;
		}
		int distance();
		void drawpoints();
		void drawline(int threshold);
};

int line::distance(CvPoint A, CvPoint B){
	int x;	
	if(A.x < B.x)
		x = B.x - A.x;
	else
		x = A.x - B.x;

	int y;	
	if(A.y < B.y)
		x = B.y - A.y;
	else
		x = A.y - B.y;
	
	int distance = (int)sqrt((x*x)+(y*y))
	return distance;
}

void line::drawpoints(){
	for(int i = 0; i < ptlist.size(); i++){
		cvLine(img, ptlist[i], ptlist[i], color, 1);
	}
}

void line::drawline(int threshold){
	for(int i = 0; i < ptlist.size() - 1; i++){
		if(distance(ptlist[i], ptlist[i+1]) < threshold){
			cvLine(img, ptlist[i], ptlist[i + 1], color, 1);
		}
	}
}
